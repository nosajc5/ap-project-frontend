import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

import { AuthService } from "../services";

@Injectable({ providedIn: 'root'})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authService.currentUserValue;
        if(currentUser) {
            if((currentUser.role == 'M') && ((state.url == "/admin/manage") || (state.url == "/admin/orders")))
                return true;
            
            if((currentUser.role == "C") && ((state.url == "/order") || (state.url == "/confirmOrder")))
                return true;
        }

        // not logged in so redirect to login page with return url
        this.router.navigate(["/login-reg"], { queryParams: { returnUrl: state.url }});
        return false;
    }
}