export interface Order {
    id: string;
    firstName: string;
    items: OrderItem[];
    amtItems: number;
    totalCost: number;
    status: string;
}

export interface OrderItem {
    id: string;
    quantity: number
    cost: number;
}