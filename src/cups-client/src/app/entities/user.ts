export class User {
    id: string;
    firstName: string;
    lastName: string;
    role: string;
    accessToken?: string;
}