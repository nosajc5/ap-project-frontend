import { MenuItem } from './MenuItem';

export const loadingMenuItems: MenuItem[] = [
    { 
        id: "000", 
        name: "Loading", 
        description: "Loading",
        category: "Loading",
        cost: 0,
        stockQuantity: 0,
        photo: {
            name: " ",
            path: "https://www.babysignlanguage.com/signs/wait.gif",
            md5Hash: "00000"
        },
        aslRep: {
            name: " ",
            path: "https://www.babysignlanguage.com/signs/wait.gif",
            md5Hash: "00000"
        },
        englishAudioRep: {
            name: " ",
            path: " ",
            md5Hash: "00000"
        },
    },

];