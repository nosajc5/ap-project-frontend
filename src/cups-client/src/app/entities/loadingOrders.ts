import { Order } from './Order';

export const loadingOrders: Order[] = [
    {
        id: "000",
        firstName: "Loading",
        items: [ 
            {
                id: "000",
                quantity: 0,
                cost: 0
            }
        ],
        amtItems: 0,
        totalCost: 0,
        status: "Loading"
    }
]