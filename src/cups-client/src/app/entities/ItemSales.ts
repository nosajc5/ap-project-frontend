export class ItemSales {
    id: number;
    totalQtySold: number;
    totalValueSold: number;
    weekStat: ItemDay[];
}

export class ItemDay {
    day: string;
    qtySold: number;
    valueSold: number;
}