import { ItemSales } from './ItemSales';

export const mockItemSales: ItemSales = {
    id: 123,
    totalQtySold: 100,
    totalValueSold: 100323,
    weekStat: [
      {
        day: "Sun",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Mon",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Tues",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Wed",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Thurs",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Fri",
        qtySold: 34,
        valueSold: 231
      },
      {
        day: "Sat",
        qtySold: 34,
        valueSold: 231
      },
    ]
  };
