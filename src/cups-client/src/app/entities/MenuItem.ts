export interface MenuItem {
    id: string;
    name: string;
    description: string;
    category: string;
    cost: number;
    stockQuantity: number;
    photo: media;
    aslRep: media;
    englishAudioRep: media;
}

interface media {
    name: string;
    path: string;
    md5Hash: string;
}