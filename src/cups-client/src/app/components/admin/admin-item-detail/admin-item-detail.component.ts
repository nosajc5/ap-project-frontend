import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuItem } from '../../../entities';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminItemUpdateComponent } from "../index";
import { MenuItemService } from 'src/app/services';

@Component({
  selector: 'app-admin-item-detail',
  templateUrl: './admin-item-detail.component.html',
  styleUrls: ['./admin-item-detail.component.scss']
})
export class AdminItemDetailComponent implements OnInit {
  item: MenuItem = this.data.selectedItem;
  update: any;
  delete: any;
  
  constructor(
    private menuItemService: MenuItemService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AdminItemDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.update = data.updateItem;
      this.delete = data.deleteItem;
      console.log(data);
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  openUpdateModal() {
    this.dialog.open(AdminItemUpdateComponent, {
      width: '35vw',
      height: '40vh',
      data: {
        itemID: this.item.id,
        updateItem: this.update
      }
    });
  }

  ngOnInit(): void {
  }

}
