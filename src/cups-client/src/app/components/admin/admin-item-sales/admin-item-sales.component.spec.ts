import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminItemSalesComponent } from './admin-item-sales.component';

describe('AdminItemSalesComponent', () => {
  let component: AdminItemSalesComponent;
  let fixture: ComponentFixture<AdminItemSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminItemSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminItemSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
