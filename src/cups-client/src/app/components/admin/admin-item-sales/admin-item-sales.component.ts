import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemSales } from '../../../entities';
import { Chart } from 'chart.js';
import { mockItemSales} from '../../../entities';

@Component({
  selector: 'app-admin-item-sales',
  templateUrl: './admin-item-sales.component.html',
  styleUrls: ['./admin-item-sales.component.scss']
})
export class AdminItemSalesComponent implements OnInit {
  salesData: ItemSales;
  isLoading: boolean;
  qtyBarchart = [];
  valueBarchart = [];
  valueSold = [];
  qtySold = [];

  constructor(
    public dialogRef: MatDialogRef<AdminItemSalesComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.isLoading = true;
     }

  closeModal(): void {
    this.dialogRef.close();
  }

  createChart(chartOptions: any): Chart {
    const { elementId, labels, data, backgroundColor } = chartOptions;

    return new Chart(elementId, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [
          {
            data: data,
            borderColor: '#3cba9f',
            backgroundColor: backgroundColor,
            fill: true
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }]
        }
      }
    });
  }

  ngOnInit(): void {
    this.salesData = mockItemSales;
    let qtySold: number[] = this.salesData.weekStat.map(res => res.qtySold);
    let valueSold: number[] = this.salesData.weekStat.map(res => res.valueSold);
    let allDates: string[] = this.salesData.weekStat.map(res => res.day);

    this.isLoading = false;

    this.qtyBarchart = this.createChart({
      elementId: 'qtyBarchartCanvas',
      labels: allDates,
      data: qtySold,
      backgroundColor:  [
        "#3cb371",  
        "#0000FF",  
        "#9966FF",  
        "#4C4CFF",  
        "#00FFFF",  
        "#f990a7",  
        "#aad2ed"
      ]
    });

    this.valueBarchart = this.createChart({
      elementId: "valueBarchartCanvas",
      labels: allDates,
      data: valueSold,
      backgroundColor:  [
        "#3cb371",  
        "#0000FF",  
        "#9966FF",  
        "#4C4CFF",  
        "#00FFFF",  
        "#f990a7",  
        "#aad2ed"
      ]
    });

  }
}
