import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpEventType, HttpErrorResponse } from "@angular/common/http";
import { of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { MenuItemService } from "../../../services";
import { MenuItem } from '../../../entities'
import { fileURLToPath } from 'url';

@Component({
  selector: 'app-admin-item-add',
  templateUrl: './admin-item-add.component.html',
  styleUrls: ['./admin-item-add.component.scss']
})
export class AdminItemAddComponent implements OnInit {
  item: MenuItem;
  public form: {
    name: string;
    category: string;
    cost: number;
    stockQuantity: number;
    unit: string;
    description: string;
    photo: FileList | null;
    aslRepresentation: FileList | null;
    englishAudioRepresentation: FileList | null;
  }

  // @ViewChild("addItem", {static: false})
  constructor(
    public dialogRef: MatDialogRef<AdminItemAddComponent>, private menuItemService: MenuItemService) { 
      this.form = {
        name: "",
        category: "",
        cost: 0,
        stockQuantity: 0,
        unit: "",
        description: "",
        photo: null,
        aslRepresentation: null,
        englishAudioRepresentation: null
      }
      
    }

  closeModal(): void {
    this.dialogRef.close();
  }


  uploadItem(file, field) {
    const formData = new FormData();
    formData.append("field", field);
    formData.append("file",file.data);
    file.inProgress = true;
    
  }

  onSubmit() {
    console.log(this.form);
    let name = this.form.name,
     category = this.form.category,
     cost = this.form.cost,
     stockQuantity = this.form.stockQuantity,
     unit = this.form.unit,
     description = this.form.description
    ;

    let photo = (this.form.photo && this.form.photo.length)
      ? this.form.photo[0]
      : null
    ;

    let aslRepresentation = (this.form.aslRepresentation && this.form.aslRepresentation.length)
      ? this.form.aslRepresentation[0]
      : null
    ;

    let englishAudioRepresentation = (this.form.englishAudioRepresentation && this.form.englishAudioRepresentation.length)
      ? this.form.englishAudioRepresentation[0]
      : null
    ;

    this.menuItemService.addItem({
      name,
      category,
      cost,
      stockQuantity,
      unit,
      description,
      photo,
      aslRepresentation,
      englishAudioRepresentation
    }).subscribe(
      value => {
        console.log("Item Created", value);
        }
      // },
      // error => {
      //   console.log("Error", error);
      // },
      // () => {
      //   console.log("Completed");
      // }
    )

    

    // const formData = new FormData();



    // this.menuItemService.addItem(formData).pipe(
    //   map(event => {
    //     switch (event.type) {
    //       case HttpEventType.UploadProgress:
            
    //     }
    //   })
    // )
  }

  ngOnInit(): void {
  }

}
