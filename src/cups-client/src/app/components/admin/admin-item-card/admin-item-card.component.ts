import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { MenuItem } from 'src/app/entities';

@Component({
  selector: 'app-admin-item-card',
  templateUrl: './admin-item-card.component.html',
  styleUrls: ['./admin-item-card.component.scss']
})
export class AdminItemCardComponent implements OnInit {
  @Input() item: MenuItem;
  @Output("viewItem") viewItem: EventEmitter<any> = new EventEmitter();
  @Output("viewSales") viewSales: EventEmitter<any> = new EventEmitter();

  constructor() { }

  view(): void {
    this.viewItem.emit(this.item);
  }

  sales(): void {
    this.viewSales.emit(this.item.id);
  }

  ngOnInit(): void {
  }

}
