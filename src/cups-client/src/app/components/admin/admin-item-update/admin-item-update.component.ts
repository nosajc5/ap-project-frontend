import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MenuItemService } from "../../../services";

@Component({
  selector: 'app-admin-item-update',
  templateUrl: './admin-item-update.component.html',
  styleUrls: ['./admin-item-update.component.scss']
})
export class AdminItemUpdateComponent implements OnInit {
  itemID: string;
  field: string;
  value: number;
  update: any;

  constructor(
    private menuItemService: MenuItemService,
    public dialogRef: MatDialogRef<AdminItemUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.itemID = data.itemID;
      this.update = data.updateItem;
    }

  ngOnInit(): void {
  }

  onSubmit() {
    this.update(this.itemID, this.field, this.value);
    this.dialogRef.close();
  }

}
