import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { MatCardModule } from '@angular/material/Card';
import { MatButtonModule } from '@angular/material/Button';
import { MatInputModule } from '@angular/material/Input';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

import { AdminItemAddComponent } from './admin/admin-item-add/admin-item-add.component';
import { AdminItemCardComponent } from './admin/admin-item-card/admin-item-card.component';
import { AdminItemDetailComponent } from './admin/admin-item-detail/admin-item-detail.component';
import { AdminItemSalesComponent } from './admin/admin-item-sales/admin-item-sales.component';
import { AdminItemUpdateComponent } from './admin/admin-item-update/admin-item-update.component';

@NgModule({
    imports: [
        CommonModule, 
        FormsModule,
        MatToolbarModule,
        MatCardModule, 
        MatButtonModule, 
        MatInputModule, 
        MatSelectModule,
        MatGridListModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatDialogModule,
        MatButtonToggleModule,
        MatTableModule,
        MatPaginatorModule,
    ],
    exports: [
        CommonModule, 
        FormsModule,
        MatToolbarModule,
        MatCardModule, 
        MatButtonModule, 
        MatInputModule, 
        MatSelectModule,
        MatGridListModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatDialogModule,
        MatButtonToggleModule,
        MatTableModule,
        MatPaginatorModule,
        AdminItemCardComponent,
        AdminItemDetailComponent,
    ], 
    declarations: [AdminItemAddComponent, AdminItemCardComponent, AdminItemDetailComponent, AdminItemSalesComponent, AdminItemUpdateComponent]
})

export class MaterialModule {}