import { Component } from '@angular/core';
import { AuthService} from "./services";
import { User } from "./entities";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cups-client';
  currentUser: User;


  constructor(
    private router: Router,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(user => { this.currentUser = user; } );
  }

  logout() {
    this.authService.signout();
  }
}