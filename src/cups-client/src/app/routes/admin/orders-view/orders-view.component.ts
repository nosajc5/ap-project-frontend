import { Component, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MenuItem, loadingMenuItems, Order, loadingOrders } from "../../../entities"
import { MenuItemService, OrderService } from "../../../services";
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-orders-view',
  templateUrl: './orders-view.component.html',
  styleUrls: ['./orders-view.component.scss'],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({height: '0px', minHeight: '0'})),
      state("expanded", style({height: '*'})),
      transition("expanded <=> collapsed", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))
    ])
  ]
})
export class OrdersViewComponent implements OnInit {
  displayedColumns: string[];
  expandedOrder: Order | null;
  detailsColumns: string[];
  menuItems: MenuItem[];
  dataSource: MatTableDataSource<Order>;
  API_BASE_URL: string;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private menuItemService: MenuItemService, private orderService: OrderService) { 
    this.displayedColumns = ["customerName", "amtItems", "totalCost", "status"];
    this.detailsColumns = ["name", "quantity", "cost"];
    this.menuItems = loadingMenuItems;
    this.dataSource =  new MatTableDataSource(loadingOrders);
    this.API_BASE_URL = environment.API_URL;
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getMenuItems();
    this.getOrders();
  }

  getMenuItems(): void {
    this.menuItemService.getAllMenuItems()
    .subscribe(
      resultArray => {
        //  Append the API base url to the media path
        this.menuItems = resultArray.map(item => {
          item.photo.path = `${this.API_BASE_URL}/storage/${item.photo.path}`;
          item.aslRep.path = `${this.API_BASE_URL}/storage/${item.aslRep.path}`;
          item.englishAudioRep.path = `${this.API_BASE_URL}/storage/${item.englishAudioRep.path}`
          return item;
        });

        console.log(this.menuItems);
      },
      error => { 
        console.log("Error: " + error);
      }
    );
  }

  getOrders(): void {
    this.orderService.getAllOrders()
    .subscribe(
      resultArray => {
        this.dataSource = new MatTableDataSource(<Order[]>resultArray);
        console.log(resultArray);
      },
      error => {
        console.log("Error " + error);
      }
    )
  }

  getItemName(itemId: string): string {
    const item = this.menuItems.find((menuItem => menuItem.id === itemId));
    
    return item.name;
  }

}
