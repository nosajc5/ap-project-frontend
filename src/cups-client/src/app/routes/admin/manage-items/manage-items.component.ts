import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuItem, loadingMenuItems } from '../../../entities';
import { AdminItemAddComponent, AdminItemDetailComponent, AdminItemSalesComponent } from '../../../components/admin';
import { MenuItemService } from "../../../services";
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-manage-items',
  templateUrl: './manage-items.component.html',
  styleUrls: ['./manage-items.component.scss']
})
export class ManageItemsComponent implements OnInit {
  menuItems: MenuItem[];
  filteredMenuItems: MenuItem[];
  selectedItem: MenuItem;
  searchFieldValue: string;
  selectedCategory: string;
  API_BASE_URL: string;

  constructor(public dialog: MatDialog, private menuItemService: MenuItemService) {
    this.menuItems = loadingMenuItems;
    this.filteredMenuItems = this.menuItems;
    this.API_BASE_URL = environment.API_URL;
  }
  
  ngOnInit(): void {
    this.getMenuItems();
  }

  getMenuItems(): void {
    this.menuItemService.getAllMenuItems()
      .subscribe(
        resultArray => {
          //  Append the API base url to the media path
          this.menuItems = resultArray.map(item => {
            item.photo.path = `${this.API_BASE_URL}/storage/${item.photo.path}`;
            item.aslRep.path = `${this.API_BASE_URL}/storage/${item.aslRep.path}`;
            item.englishAudioRep.path = `${this.API_BASE_URL}/storage/${item.englishAudioRep.path}`
            return item;
          });
          //  Assign all retrieved item to filtered list
          this.filteredMenuItems = this.menuItems;
          console.log(this.menuItems);
        },
        error => { 
          console.log("Error: " + error);
        }
      );
  }

  textSearch() {
    //  Check if string is empty and then get all menu items
    if(this.searchFieldValue.length < 1)
      this.getMenuItems();
    else { 
      //  Compares the value of the search field with the name of the each item and filter list
      this.filteredMenuItems = this.menuItems.filter(
        item => item.name.toLowerCase().includes(this.searchFieldValue.toLowerCase())
      );
    }
  }

  clearSearchField() {
    this.searchFieldValue = "";
  }

  categorySearch() {
    //  Check if "all" is selected as the selected category and gets all menu items
    if(this.selectedCategory.toLowerCase() == "all")
      this.getMenuItems();
    else {
      this.filteredMenuItems = this.menuItems.filter(
        item => item.category.toLowerCase() == this.selectedCategory.toLowerCase()
        //item => item.category.toLowerCase().includes(this.selectedCategory.toLowerCase())
      );
    }
  }
  
  addItem(): void {
    const dialogRef = this.dialog.open(AdminItemAddComponent, {
      width: '65vw',
      height: '90vh'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getMenuItems();
    }) 
  }

  deleteItem(id: string) {
    this.menuItemService.deleteMenuItem(id).subscribe(
      result => { 
        console.log(result); 
      },
      error => { console.log(error); }
    );
  }

  updateItem(itemID: string, field: string, value: number) {
    this.menuItemService.updateMenuItem(itemID, field, value).subscribe(
      result => {
        console.log(result);
      },
      error => { console.log(error); }
    );
  }

  viewItem(item: MenuItem) {
    this.selectedItem = item;
    const dialogRef = this.dialog.open(AdminItemDetailComponent, {
      width: '65vw',
      height: '90vh',
      data: {
        selectedItem: this.selectedItem,
        updateItem: this.updateItem,
        deleteItem: this.deleteItem
      }
    });
    
    console.log(this.selectedItem);

    dialogRef.afterClosed().subscribe(() => {
      this.getMenuItems();
    }) 
  }

  viewSales(itemId: number) {
    // Todo: get sales data and pass to component
    this.dialog.open(AdminItemSalesComponent, {
      width: '65vw',
      height: '90vh',
      data: itemId
    });

    console.log(itemId);
  }
  
}
