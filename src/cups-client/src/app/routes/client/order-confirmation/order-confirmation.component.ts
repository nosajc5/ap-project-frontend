import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

import { 
  loadingOrders, 
  loadingMenuItems,
  Order,
  MenuItem,
  OrderItem
} from "../../../entities";
import { MenuItemService } from '../../../services';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.scss']
})
/**@Component({
  selector: 'table-basic-example',
  styleUrls: ['table-basic-example.css'],
  templateUrl: 'table-basic-example.html',
})*/
export class OrderConfirmationComponent implements OnInit {
  displayedColumns: string[] = ['name', 'quantity', 'cost'];
  dataSource: MatTableDataSource<OrderItem>;
  order: Order;
  menuItems: MenuItem[];

  constructor(private menuItemService: MenuItemService) { 
    this.menuItems = loadingMenuItems;
    this.order = loadingOrders[0];
    this.dataSource = new MatTableDataSource(this.order.items);
  }

  ngOnInit(): void {
    console.log("Order Init");
  }

  getMenuItem(): void {
    this.menuItemService.getAllMenuItems()
    .subscribe(
      resultArray => {
        //  Append the API base url to the media path
        this.menuItems = resultArray;

        console.log(this.menuItems);
      },
      error => { 
        console.log("Error: " + error);
      }
    );
  }

  getItemName(itemID: string): string {
    const item = this.menuItems.find((menuItem => menuItem.id == itemID));
    
    return item.name;
  }

  getTotalCost(): number {
    return this.order.totalCost;
  }
}
