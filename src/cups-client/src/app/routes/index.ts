export { LoginRegComponent } from './login-reg/login-reg.component';
export { ManageItemsComponent } from './admin/manage-items/manage-items.component';
export { OrdersViewComponent } from './admin/orders-view/orders-view.component';
export { OrderConfirmationComponent } from './client/order-confirmation/order-confirmation.component';
