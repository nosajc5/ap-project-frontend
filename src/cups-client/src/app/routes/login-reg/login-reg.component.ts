import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth/auth.service';
import { first } from "rxjs/operators";
import { User } from 'src/app/entities';

@Component({
  selector: 'app-login-reg',
  templateUrl: './login-reg.component.html',
  styleUrls: ['./login-reg.component.scss']
})
export class LoginRegComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  // User data
  firstName: string;
  lastName: string;
  password: string;  
  signatureMoveImageFile: File;
  voiceRecordingAudioFile: File;  

  registerFlag: boolean;
  showSpinner: boolean;
  error: boolean;
  submitted: boolean;

  //  Digitial ID Section Attributes
  digitalIdOptions: string[] = ["Password", "Signature Move", "Voice"];
  digitalIdOptionsToggle: boolean[] = [false, false, false];
  selectedIdType: string = "Password";

  ngOnInit(): void {
    this.authService.signout();
    this.showSpinner = false;
    this.registerFlag = false;
    this.digitalIdOptionsToggle[0] = true;
    this.submitted = false;
  }

  toggleLoginRegistFlag() {
    this.registerFlag = !this.registerFlag;
  }

  toggleSelectedDigitalId({ value }) {
    const selection: string = value;

    this.digitalIdOptions.forEach((option, index) => {
      // console.log(option);
      if(option == selection) {
        this.digitalIdOptionsToggle[index] = true;
        this.selectedIdType = this.digitalIdOptions[index];
        console.log(this.selectedIdType);
        // console.log(option);
      }
      else {
        this.digitalIdOptionsToggle[index] = false;
        // console.log(option);
      }
    });
  }

  fileChange({ target: { files, name } }) {
    if(files.length > 0)
      this[name] = files[0];
  }

  onSubmit() {
    if(this.registerFlag)
      this.register();
    else
      this.login();
  }

  login() {
    this.showSpinner = true;

    const loginData = {
      firstName: this.firstName,
      lastName: this.lastName,
      idType: "Password",
      password: this.password
    };

    this.authService.signIn(loginData)
      .pipe(first())
      .subscribe(
        (data: User) => {
          console.log(data);
          console.log("Logged in!");
          if(data.role == "C")
            this.router.navigate(["/order"]);
          if(data.role == "M") 
            this.router.navigate(["/manage"]);
        },
        error => {
          this.error = error;
          this.showSpinner = true;
          console.log(this.error);
        }
      );
  }

  register() {
    this.showSpinner = true;

    const registerData = {
      firstName: this.firstName,
      lastName: this.lastName,
      idType: "Password",
      password: this.password
    };

    console.log("Registered");
    this.authService.register(registerData)
      .pipe(first())
      .subscribe(
          data => {
            this.showSpinner = false;
            this.toggleLoginRegistFlag();
          },
          error => {
            // handle error
        }
      );
  }
}
