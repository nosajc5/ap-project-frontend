import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginRegComponent, ManageItemsComponent, OrdersViewComponent, OrderConfirmationComponent } from './routes';
import { AuthGuard } from "./helpers";

const routes: Routes = [
  { path: 'login-reg', component: LoginRegComponent },
  { path: 'admin/manage', component: ManageItemsComponent, canActivate: [AuthGuard] },
  { path: 'admin/orders', component: OrdersViewComponent, canActivate: [AuthGuard] },
  { path: "orderConfirm", component: OrderConfirmationComponent, canActivate: [AuthGuard] },
  { 
    path: '',
    redirectTo: '/login-reg',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    redirectTo: '/admin/manage',
    pathMatch: 'full'
  },
  {
    path: 'manage',
    redirectTo: '/admin/manage',
    pathMatch: 'full'
  },

  // Redirect to login
  { path: "**", component: LoginRegComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
