import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { MenuItem } from "../entities";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuItemService {
  API_BASE_URL: string;

  constructor(private httpClient: HttpClient) { 
    this.API_BASE_URL = environment.API_URL;
  }

  public addItem(itemData) {
    const formData = new FormData();

    formData.append("name", itemData.name);
    formData.append("category", itemData.category);
    formData.append("cost", itemData.cost);
    formData.append("stockQuantity", itemData.stockQuantity);
    formData.append("unit", itemData.unit);
    formData.append("description", itemData.description);
    formData.append("photo", itemData.photo);
    formData.append("aslRepresentation", itemData.aslRepresentation);
    formData.append("englishAudioRepresentation", itemData.englishAudioRepresentation);

    console.log(`Adding Menu Item - ${formData}`);  //  Log form data that's being sent to the api

    return this.httpClient.post<any>(`${this.API_BASE_URL}/menuItem/`, formData, {
      reportProgress: true,
      observe: "events"
    });
  }

  public getAllMenuItems() {
    console.log("Getting all menu items");
    return this.getItems("menuItem/all");
  }

  private getItems(route: string): Observable<MenuItem[]> {
    return this.httpClient.get<MenuItem[]>(`${this.API_BASE_URL}/${route}`);
  }

  public updateMenuItem(itemID: string, key: string, value: number) {
    console.log(`Update Menu Item - itemID: ${itemID}, key: ${key}, value: ${value}`);

    return this.httpClient.patch(`${this.API_BASE_URL}/menuItem/item/${itemID}`, {key, value}, {
      reportProgress: true,
      observe: "events"
    });
  }

  public deleteMenuItem(itemID: string) {
    console.log(`Delete Menu Item - ${itemID}`)
    return this.httpClient.delete(`${this.API_BASE_URL}/menuItem/item/${itemID}`, {
      reportProgress: true,
      observe: "events"
    });
  }
  
}