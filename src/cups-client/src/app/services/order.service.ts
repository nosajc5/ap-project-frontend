import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { Order } from "../entities";

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  API_BASE_URL: string;

  constructor(private httpClient: HttpClient) { 
    this.API_BASE_URL = environment.API_URL;
  }

  public saveOrder(order: any) {
    return this.httpClient.post(`${this.API_BASE_URL}/order/`, order, {
      reportProgress: true,
      observe: "events"
    });
  }

  public getAllOrders() {
    return this.httpClient.get<Order[]>(`${this.API_BASE_URL}/order/all`);
  }

  public getSpecifcOrder(userID: string) {
    return this.httpClient.get<Order>(`${this.API_BASE_URL}/specificOrder/${userID}`);
  }

  public updateOrder(orderID: string, field: string, value: number) {
    return this.httpClient.patch(`${this.API_BASE_URL}/order/${orderID}`, {});
  }

  public deleteOrder(orderID: string) {
    return this.httpClient.delete(`${this.API_BASE_URL}/order/${orderID}`);
  }

}
