import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from "rxjs/operators";
import { Router } from '@angular/router';

import { environment } from "../../../environments/environment";
import { User } from '../../entities';

const httpsOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API_BASE_URL: string;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private httpClient: HttpClient, public router: Router) { 
    // Assign base URL from API
    this.API_BASE_URL = environment.API_URL;

    // Check if the local storage is empty and assign a dummy user as the current user
    if(localStorage.getItem("currentUser") == null)
      this.currentUserSubject = new BehaviorSubject<User>({
        id: " ",
        firstName: " ",
        lastName: " ",
        role: " ",
      });
    else  // If there's a user in the local storage, assign that user as the current user
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem("currentUser")));

    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  register(user: any) {
    return this.httpClient.post(`${this.API_BASE_URL}/user/register`, user);
  }

  signIn(loginInfo: any) {
    return this.httpClient.post<any>(`${this.API_BASE_URL}/user/login`, loginInfo)
      .pipe(map((user: User) => {
        // Login successful if there's a JWT token in the response
        if(user && user.accessToken) {
          // Store user details and JWT token in local storage to keep user logged in between page refreshes
          localStorage.removeItem("currentUser");
          localStorage.setItem("currentUser", JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        console.log(user);
        return user;
      }));
  }

  signout() {
    localStorage.removeItem("currentUser");
    this.currentUserSubject.next({
      id: " ",
      firstName: " ",
      lastName: " ",
      role: " ",
    });
    this.router.navigate(["/login-reg"]);
  }
}
